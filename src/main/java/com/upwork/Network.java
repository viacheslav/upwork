package com.upwork;

import com.upwork.util.UnionFind;

/**
 * The class uses classical UnionFind (UF) solution.
 * <p>
 * As UF is based on arrays with start index at 0,
 * {@link Network} class decreases node number by 1 before passing to UF.
 */
public class Network {

    private final UnionFind unionFind;
    private final int elementsNumber;

    public Network(int elementsNumber) {
        if (elementsNumber <= 0) {
            throw new IllegalArgumentException("Number of elements should be more then 0.");
        }
        this.unionFind = new UnionFind(elementsNumber);
        this.elementsNumber = elementsNumber;
    }

    public void connect(int node1, int node2) {
        validateNode(node1);
        validateNode(node2);

        int node1TransformedForUF = node1 - 1;
        int node2TransformedForUF = node2 - 1;

        if (!unionFind.connected(node1TransformedForUF, node2TransformedForUF)) {
            unionFind.union(node1TransformedForUF, node2TransformedForUF);
        }
    }

    public boolean query(int node1, int node2) {
        validateNode(node1);
        validateNode(node2);

        int node1TransformedForUF = node1 - 1;
        int node2TransformedForUF = node2 - 1;

        return unionFind.connected(node1TransformedForUF, node2TransformedForUF);
    }

    private void validateNode(int p) {
        if (p <= 0 || p > elementsNumber) {
            throw new IllegalArgumentException("Node number should be more than 0 and not more than number of " +
                    "elements.");
        }
    }

}
