package com.upwork;

import org.junit.Assert;
import org.junit.Test;

public class NetworkTest {

    @Test
    public void test() {
        // given
        Network network = new Network(8);

        network.connect(1, 2);
        network.connect(6, 2);
        network.connect(2, 4);
        network.connect(5, 8);

        // assert
        Assert.assertTrue(network.query(1, 2));
    }

    @Test
    public void test1() {
        // given
        Network network = new Network(3);

        network.connect(1, 2);
        network.connect(2, 3);
        network.connect(1, 3);

        // assert
        Assert.assertTrue(network.query(1, 3));
    }

    @Test
    public void test2() {
        // given
        Network network = new Network(3);

        network.connect(1, 2);

        // assert
        Assert.assertTrue(network.query(1, 2));
    }

    @Test
    public void test3() {
        // given
        Network network = new Network(3);

        network.connect(1, 2);
        network.connect(2, 3);

        // assert
        Assert.assertTrue(network.query(1, 3));
    }

    @Test
    public void test4() {
        // given
        Network network = new Network(4);

        network.connect(1, 2);
        network.connect(2, 3);
        network.connect(3, 4);

        // assert
        Assert.assertTrue(network.query(1, 4));
    }

    @Test
    public void test5() {
        // given
        Network network = new Network(3);

        network.connect(2, 3);

        // assert
        Assert.assertFalse(network.query(1, 2));
    }

    @Test
    public void test6() {
        // given
        Network network = new Network(4);

        network.connect(1, 2);
        network.connect(2, 3);
        network.connect(1, 3);

        // assert
        Assert.assertFalse(network.query(1, 4));
    }

    @Test
    public void test7() {
        // given
        Network network = new Network(4);

        network.connect(1, 2);
        network.connect(3, 4);

        // assert
        Assert.assertFalse(network.query(2, 4));
    }

    @Test
    public void test8() {
        // given
        Network network = new Network(4);

        network.connect(1, 2);
        network.connect(3, 2);
        network.connect(1, 3);
        network.connect(1, 3);


        // assert
        Assert.assertFalse(network.query(1, 4));
    }

    @Test
    public void test9() {
        // given
        Network network = new Network(4);

        network.connect(1, 2);
        network.connect(2, 3);
        network.connect(1, 3);
        network.connect(4, 4);


        // assert
        Assert.assertFalse(network.query(1, 4));
    }

    @Test(expected = IllegalArgumentException.class)
    public void test10() {
        // given
        Network network = new Network(4);

        network.connect(1, 5);

        // assert: exception

    }

    @Test(expected = IllegalArgumentException.class)
    public void test11() {
        // given
        Network network = new Network(0);

        // assert: exception

    }

    @Test(expected = IllegalArgumentException.class)
    public void test12() {
        // given
        Network network = new Network(-1);

        // assert: exception

    }

    @Test(expected = IllegalArgumentException.class)
    public void test13() {
        // given
        Network network = new Network(4);

        network.connect(1, 4);

        // assert: exception
        network.query(1, 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test14() {
        // given
        Network network = new Network(4);

        network.connect(1, 4);

        // assert: exception
        network.query(5, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test15() {
        // given
        Network network = new Network(4);

        network.connect(0, 4);

        // assert: exception
    }

    @Test(expected = IllegalArgumentException.class)
    public void test16() {
        // given
        Network network = new Network(4);

        network.connect(1, 4);

        // assert: exception
        network.query(0, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test17() {
        // given
        Network network = new Network(4);

        network.connect(4, 0);

        // assert: exception
    }

    @Test(expected = IllegalArgumentException.class)
    public void test18() {
        // given
        Network network = new Network(4);

        network.connect(1, 4);

        // assert: exception
        network.query(1, 0);
    }


}